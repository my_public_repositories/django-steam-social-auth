from django.urls import re_path
from django.contrib.auth.decorators import login_required

from authentication.views import IndexView, LogoutView

urlpatterns = [
    re_path(r"^$", IndexView.as_view(), name="index"),
    re_path(r"^logout", login_required(LogoutView.as_view(), login_url="/"), name="logout"),
]
